@subheading gsasl_property_free
@anchor{gsasl_property_free}
@deftypefun {void} {gsasl_property_free} (Gsasl_session * @var{sctx}, Gsasl_property @var{prop})
@var{sctx}: session handle.

@var{prop}: enumerated value of @code{Gsasl_property}  type to clear

Deallocate associated data with property  @code{prop} in session handle.
After this call, gsasl_property_fast( @code{sctx} ,  @code{prop} ) will always
return NULL.

@strong{Since:} 2.0.0
@end deftypefun

