# GNU SASL CONTRIBUTING -- Information for developers
Copyright (C) 2009-2024 Simon Josefsson
See the end for copying conditions.

This file contains instructions for developers and advanced users that
wants to build from version controlled sources.

## Obtaining sources

Download the version controlled sources:
```
$ git clone https://gitlab.com/gsasl/gsasl.git
$ cd gsasl
```

## Dependencies

GNU SASL requires several tools to build the software, including:

- Automake <http://www.gnu.org/software/automake/>
- Autoconf <http://www.gnu.org/software/autoconf/>
- Libtool <http://www.gnu.org/software/libtool/>
- Gettext <http://www.gnu.org/software/gettext/>
- Texinfo <http://www.gnu.org/software/texinfo/>
- Gperf <http://www.gnu.org/software/gperf/>
- help2man <http://www.gnu.org/software/help2man/>
- Gengetopt <http://www.gnu.org/software/gengetopt/>
- Tar <http://www.gnu.org/software/tar/>
- Gzip <http://www.gnu.org/software/gzip/>
- Texlive & epsf <http://www.tug.org/texlive/> (for PDF manual)
- CVS <http://www.gnu.org/software/cvs/> (for gettext autopoint)
- GTK-DOC <http://www.gtk.org/gtk-doc/> (for API manual)
- Doxygen <http://www.stack.nl/~dimitri/doxygen/> (for API manual)
- Git <http://git.or.cz/>
- Perl <http://www.cpan.org/>
- Valgrind <http://valgrind.org/> (optional)
- dia <http://live.gnome.org/Dia/> (for images in manual)
- GnuTLS <http://www.gnu.org/software/gnutls/> (for TLS support)

The software is typically distributed with your operating system, and
the instructions for installing them differ.  Here are some hints:

Debian, Ubuntu, Trisquel:
```
apt install git autoconf automake libtool autopoint gettext cvs make
apt install texinfo texlive texlive-plain-generic texlive-extra-utils texlive-font-utils
apt install help2man gtk-doc-tools valgrind
apt install dia libgnutls28-dev gengetopt gperf
```

Apple Mac:
```
brew install autoconf automake wget libtool gengetopt help2man
```

## Building the project

Build the project as follows:

```
$ ./bootstrap
$ ./configure --enable-gtk-doc --enable-gtk-doc-pdf --enable-gcc-warnings --enable-valgrind-tests
$ make
$ make check
```

## Continuous Integration

The project is built auomatically on every git commit using GitLab
CI/CD, see the file `.gitlab-ci.yml` for rules and [current
pipeline](https://gitlab.com/gsasl/gsasl/-/pipelines).

## Valgrind

For various reasons, you may run into valgrind false positives that
will cause self-checks to fail.  We ship a Valgrind suppression file
to address common issues.  You can use it by putting the following in
your ~/.valgrindrc:

```
--suppressions=/path/to/gsasl/tests/libgsasl.supp
```

If using valgrind on self-tests causes problems you cannot solve, try
./configure --disable-valgrind-tests to avoid using it.

## Release process

Read README-release on how to prepare a new release.  The file is
generated by running ./bootstrap, see above on building.

To prepare releases you need some additional tools:

```
sudo apt-get install pmccabe mingw-w64 wine clang lcov doxygen ncftp
```

Happy hacking!

----------------------------------------------------------------------
Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
